class CreatePublications < ActiveRecord::Migration
  def change
    create_table :publications do |t|
      t.string :title
      t.string :author
      t.string :conferenceName
      t.string :location
      t.string :year

      t.timestamps
    end
  end
end
