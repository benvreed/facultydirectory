class AddMemberIdToPublications < ActiveRecord::Migration
  def change
    add_column :publications, :member_id, :integer
  end
end
