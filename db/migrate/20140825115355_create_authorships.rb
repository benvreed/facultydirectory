class CreateAuthorships < ActiveRecord::Migration
  def change
    create_table :authorships do |t|
      t.integer :member_id
      t.integer :publication_id

      t.timestamps
    end
  end
end
