class CreateMembers < ActiveRecord::Migration
  def change
    create_table :members do |t|
      t.string :lastName
      t.string :firstName
      t.string :position

      t.timestamps
    end
  end
end
