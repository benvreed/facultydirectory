class RemoveAuthorFromPublications < ActiveRecord::Migration
  def change
    remove_column :publications, :author
  end
end
