class ChangeProductIdToPublicationIdInJoinTable < ActiveRecord::Migration
  def change
    add_column :members_publications, :publication_id, :integer
    remove_column :members_publications, :product_id
  end
end
