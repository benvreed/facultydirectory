class CreateMembersPublicationsTable < ActiveRecord::Migration
  def change
    create_table :members_publications do |t|
      t.column :member_id, :integer
      t.column :product_id, :integer
    end
  end
end
