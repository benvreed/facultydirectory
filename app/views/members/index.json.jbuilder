json.array!(@members) do |member|
  json.extract! member, :id, :lastName, :firstName, :position
  json.url member_url(member, format: :json)
end
