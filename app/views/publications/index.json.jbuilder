json.array!(@publications) do |publication|
  json.extract! publication, :id, :title, :author, :conferenceName, :location, :year
  json.url publication_url(publication, format: :json)
end
